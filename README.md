Table of Contents
=================

* [Table of Contents](#table-of-contents)
   * [Overview](#overview)
   * [Sourcing example](#sourcing-example)
   * [Inputs](#inputs)
   * [Explanation of modules](#explanation-of-modules)

<!-- Created by https://github.com/ekalinin/github-markdown-toc -->

## Overview
Deploys flux and a BattleForge distribution of Platform-One BigBang.

## Sourcing example

The below is a minimal working example with the required variables. 
See [Inputs](#inputs) section below for more information.
If the Kubernetes cluster was deployed through the https://github.boozallencsn.com/dbp-devsecops/module-k3s-aws or https://github.boozallencsn.com/dbp-devsecops/module-rke2-aws modules, you can use the outputs of those modules for the authentication parameters.
```terraform
module "bigbang" {
  source                                 = "git::ssh://git@github.boozallencsn.com/dbp-devsecops/module-bigbang"
  cluster_domain                         = "test.dbp.sh"
  kubernetes_auth_host                   = "https://${module.kubernetes.api_lb_dns_name}:6443"
  kubernetes_auth_cluster_ca_certificate = module.kubernetes.server_ca_crt
  kubernetes_auth_client_certificate     = module.kubernetes.client_crt_combined
  kubernetes_auth_client_key             = module.kubernetes.client_admin_key
  kubeconfig_raw                         = module.kubernetes.kubeconfig
  aws_region                             = "us-gov-east-1"
  existing_s3_bucket                     = "battleforge"
  tls_crt_path                           = "test.dbp.sh/tls.crt"
  tls_key_path                           = "test.dbp.sh/tls.key"
  registry_username                      = "John_Smith"
  registry_password                      = "PLACEHOLDER"
}
```


See the Inputs section for additional information about variables.

## Inputs

| Required variable inputs               | Description |
| -----------------------------------    | ----------- |
| cluster_domain                         | FQDN of cluster. Used as domain for ingress resources. Must match certificates.
| existing_s3_bucket                     | Name of existing s3 bucket that contains tls certificates for the domain
| aws_region                             | AWS region containing the s3 bucket specified in `existing_s3_bucket`
| tls_crt_path                           | Path inside of `existing_s3_bucket` containing the server certificate. This certificate should contain the full certificate chain, in the correct order, to prevent SSL errors when clients connect: first the server certificate, then all intermediate certificates, and finally the root CA.
| tls_key_path                           | Path inside of `existing_s3_bucket` containing the server key.
| registry_username                      | Username for ironbank registry
| registry_password                      | Password for ironbank registry
| kubernetes_auth_host                   | The hostname (in form of URI) of the Kubernetes API
| kubernetes_auth_cluster_ca_certificate | PEM-encoded root certificates bundle for TLS authentication
| kubernetes_auth_client_certificate     | PEM-encoded client certificate for TLS authentication
| kubernetes_auth_client_key             | PEM-encoded client certificate key for TLS authentication
| kubeconfig_raw                         | Entire formatted kubeconfig file for authentication. Used by `Kustomization` provider.

| Optional variable inputs            | Description |
| ----------------------------------- | ----------- |
| sso_certificate                     | Copy/Paste of the public certificate of the keycloak realm. Leave this value blank for initial terraform deployment and update once keycloak gets deployed. This is needed by `SonarQube` to allow authentication with `Keycloak`.
| registry_server                     | Docker registry where ironbank containers are stored
| gatekeeper_allowed_repos            | List of allowed container registries through gatekeeper. Gatekeeper only allows the kubernetes cluster to run containers from whitelisted repositories. See default values for this variable in `variables.tf` file to see what is allowed without modifications.


## Explanation of modules
Each terraform module/submodule in this repository is inside of a directory that bears the name of the module/submodule with the prefix "module-" The proper name of the module in terraform does not contain the "module-" prefix. It is only applied to the directory name for the sake of clarity. The modules listed and explained below contain the proper name of the module and can be found in its respective directory. 

| Module Name                         | Purpose/Description |
| ----------------------------------- | ------------------- |
| flux                                | Deploys flux using the Kustomization templates provided by BigBang at the configured version.
| kubernetes                          | Deploys required Kubernetes components for BigBang and deploys BigBang through helm.
