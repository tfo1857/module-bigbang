locals {
  separator = var.developer != "" ? "-" : ""
}

module "flux" {
  source            = "./modules/flux"
  bigbang_version   = var.bigbang_version
  registry_server   = var.registry_server
  registry_username = var.registry_username
  registry_password = var.registry_password
}

module "github" {
  depends_on               = [module.kubernetes]
  source                   = "./modules/github"
  bigbang_version          = var.bigbang_version
  bigbang_services         = var.bigbang_services
  combined_pem             = var.combined_pem # the combined pem contains the tls crt and the ca cert (locally signed and self signed combined)
  developer                = var.developer
  domain_name              = var.domain_name
  gatekeeper_allowed_repos = var.gatekeeper_allowed_repos
  github_baseurl           = var.github_baseurl
  github_environment_dir   = var.github_environment_dir
  github_organization      = var.github_organization
  github_project_name      = var.github_project_name
  github_token             = var.github_token
  github_username          = var.github_username
  registry_server          = var.registry_server
  registry_username        = var.registry_username
  registry_password        = var.registry_password
  separator                = local.separator
  sso_certificate          = var.sso_certificate
  tls_key                  = var.tls_key # this is the tls key made for the tls cert
  vault_license            = var.vault_license
  realm_name               = var.realm_name
  gitlab_client_secret     = var.gitlab_client_secret
}

module "kubernetes" {
  depends_on               = [module.flux]
  source                   = "./modules/kubernetes"
  aws_access_key           = var.aws_access_key
  aws_default_region       = var.aws_default_region
  aws_secret_key           = var.aws_secret_key
  bigbang_ca               = var.bigbang_ca
  bigbang_version          = var.bigbang_version
  bucket                   = var.bucket
  bucket_dir               = var.bucket_dir
  combined_pem             = var.combined_pem # the combined pem contains the tls crt and the ca cert (locally signed and self signed combined)
  developer                = var.developer
  domain_name              = var.domain_name
  gatekeeper_allowed_repos = var.gatekeeper_allowed_repos
  github_baseurl           = var.github_baseurl
  github_environment_dir   = var.github_environment_dir
  github_organization      = var.github_organization
  github_project_name      = var.github_project_name
  github_token             = var.github_token
  github_username          = var.github_username
  kubeconfig               = var.kubeconfig
  registry_server          = var.registry_server
  registry_username        = var.registry_username
  registry_password        = var.registry_password
  separator                = local.separator
  sso_certificate          = var.sso_certificate
  tls_key                  = var.tls_key # this is the tls key made for the tls cert
  vault_license            = var.vault_license
} 
