terraform {
  required_providers {
    kustomization = {
      source = "kbst/kustomization"
    }
    github = {
      source  = "integrations/github"
      version = "5.18.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.14.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.1"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.9.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.18.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.54.0"
    }
    curl = {
      source  = "anschoewe/curl"
      version = "1.0.2"
    }
  }
}
