terraform {
  required_providers {
    kustomization = {
      source = "kbst/kustomization"
    }
    github = {
      source  = "integrations/github"
      version = "5.11.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.14.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "3.4.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.1"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.9.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.18.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "4.55.0"
    }
    curl = {
      source  = "anschoewe/curl"
      version = "1.0.2"
    }
  }
}

provider "helm" {
  kubernetes {
    host                   = yamldecode(data.aws_s3_object.grab_kubecfg.body).clusters[0].cluster.server
    client_certificate     = base64decode(yamldecode(data.aws_s3_object.grab_kubecfg.body).users[0].user.client-certificate-data)
    client_key             = base64decode(yamldecode(data.aws_s3_object.grab_kubecfg.body).users[0].user.client-key-data)
    cluster_ca_certificate = base64decode(yamldecode(data.aws_s3_object.grab_kubecfg.body).clusters[0].cluster.certificate-authority-data)
  }
}

provider "kustomization" {
  kubeconfig_raw = data.aws_s3_object.grab_kubecfg.body
}

provider "kubernetes" {
  host                   = yamldecode(data.aws_s3_object.grab_kubecfg.body).clusters[0].cluster.server
  client_certificate     = base64decode(yamldecode(data.aws_s3_object.grab_kubecfg.body).users[0].user.client-certificate-data)
  client_key             = base64decode(yamldecode(data.aws_s3_object.grab_kubecfg.body).users[0].user.client-key-data)
  cluster_ca_certificate = base64decode(yamldecode(data.aws_s3_object.grab_kubecfg.body).clusters[0].cluster.certificate-authority-data)
}

provider "kubectl" {
  host                   = yamldecode(data.aws_s3_object.grab_kubecfg.body).clusters[0].cluster.server
  client_certificate     = base64decode(yamldecode(data.aws_s3_object.grab_kubecfg.body).users[0].user.client-certificate-data)
  client_key             = base64decode(yamldecode(data.aws_s3_object.grab_kubecfg.body).users[0].user.client-key-data)
  cluster_ca_certificate = base64decode(yamldecode(data.aws_s3_object.grab_kubecfg.body).clusters[0].cluster.certificate-authority-data)
  load_config_file       = false
}

provider "aws" {
  region = var.aws_default_region
}

provider "github" {
  token    = var.github_token
  base_url = var.github_baseurl
  owner    = var.github_organization
}