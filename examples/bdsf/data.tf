data "aws_s3_object" "tls_crt" {
  count  = var.environment == "development" ? 0 : 1
  bucket = var.bucket
  key    = "${var.bucket_dir}/ssl.crt"
}

data "aws_s3_object" "tls_key" {
  count  = var.environment == "development" ? 0 : 1
  bucket = var.bucket
  key    = "${var.bucket_dir}/private.key"
}

data "aws_s3_object" "ca_crt" {
  count  = var.environment == "development" ? 0 : 1
  bucket = var.bucket
  key    = "${var.bucket_dir}/ca.crt"
}

data "aws_s3_object" "combined_pem" {
  count  = var.environment == "development" ? 0 : 1
  bucket = var.bucket
  key    = "${var.bucket_dir}/combined.pem"
}

data "aws_s3_object" "grab_kubecfg" {
  bucket = var.bucket
  key    = "${var.bucket_dir}/${var.kubeconfig}"
}

data "curl" "get_ingress_certs" {
  http_method = "GET"
  uri         = "https://repo1.dso.mil/platform-one/big-bang/bigbang/-/raw/master/chart/ingress-certs.yaml"
}
