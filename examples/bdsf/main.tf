locals {
  combined_pem = var.environment == "development" ? yamldecode(data.curl.get_ingress_certs.response).istio.gateways.public.tls.cert : data.aws_s3_object.combined_pem[0].body
  tls_key      = var.environment == "development" ? yamldecode(data.curl.get_ingress_certs.response).istio.gateways.public.tls.key : data.aws_s3_object.tls_key[0].body
}

module "bigbang" {
  source                   = "../../"
  aws_access_key           = var.aws_access_key
  aws_default_region       = var.aws_default_region
  aws_secret_key           = var.aws_secret_key
  bigbang_services         = var.bigbang_services
  bigbang_version          = var.bigbang_version
  bucket                   = var.bucket
  bucket_dir               = var.bucket_dir
  combined_pem             = local.combined_pem
  developer                = var.developer
  domain_name              = var.domain_name
  gatekeeper_allowed_repos = var.gatekeeper_allowed_repos
  github_baseurl           = var.github_baseurl
  github_environment_dir   = var.github_environment_dir
  github_organization      = var.github_organization
  github_project_name      = var.github_project_name
  github_token             = var.github_token
  github_username          = var.github_username
  kubeconfig               = var.kubeconfig
  registry_server          = var.registry_server
  registry_username        = var.registry_username
  registry_password        = var.registry_password
  tls_key                  = local.tls_key
  sso_certificate          = var.sso_certificate
  vault_license            = var.vault_license
}

output "bigbang_out" {
  value = module.bigbang
}