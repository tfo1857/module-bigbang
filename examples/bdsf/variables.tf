variable "aws_access_key" {
  description = "AWS IAM Access key"
  type        = string
}

variable "aws_default_region" {
  description = "AWS Default Region"
  type        = string
}

variable "aws_secret_key" {
  description = "AWS IAM Secret key"
  type        = string
}

variable "bigbang_services" {
  description = "Map of bigbang services we want to deploy (false or true)"
  type        = map(any)
}

variable "bigbang_version" {
  description = "BigBang version to deploy"
  default     = "1.53.0"
  type        = string
}

variable "bucket" {
  description = "AWS S3 Bucket"
  type        = string
}

variable "bucket_dir" {
  description = "AWS S3 Bucket directory"
  default     = ""
  type        = string
}

variable "developer" {
  description = "Developer running this terraform code"
  default     = ""
  type        = string
}

variable "domain_name" {
  description = "Domain name to use for BigBang services"
  type        = string
}

variable "environment" {
  description = "Environment we're deploying too (production, development, etc)"
  default     = "development"
  type        = string
}

variable "gatekeeper_allowed_repos" {
  description = "Repositories gatekeeper will allow you to use for pulling docker images"
  default = [
    "registry1.dso.mil",
    "registry.dso.mil",
    "docker.io",
    "registry.gitlab.com",
    "gcr.io",
    "releases-docker.jfrog.io",
  ]
  type = list(string)
}

variable "github_username" {
  description = "Github username"
  type        = string
}

variable "github_token" {
  description = "Github token"
  type        = string
}

variable "github_baseurl" {
  description = "Github base url (https://github.boozallencsn.com)"
  default     = "https://github.boozallencsn.com"
  type        = string
}

variable "github_organization" {
  description = "Github organization name"
  default     = "dbp-devsecops"
  type        = string
}

variable "github_project_name" {
  description = "Github project name (repository name)"
  default     = "DBP-BigBang"
  type        = string
}

variable "github_environment_dir" {
  description = "Github directory to use within the github project above"
  type        = string
}

variable "kubeconfig" {
  description = "Name of the kubeconfig in the S3 bucket"
  type        = string
}

variable "registry_username" {
  description = "Registry1 username"
  type        = string
}

variable "registry_password" {
  description = "Registry1 password (token)"
  type        = string
}

variable "registry_server" {
  description = "Registry1 server domain name"
  default     = "registry1.dso.mil"
  type        = string
}

variable "sso_certificate" {
  description = "SSO certificate from Keycloak to pass in to services"
  default     = ""
  type        = string
}

variable "vault_license" {
  description = "License for vault"
  default     = ""
  type        = string
}