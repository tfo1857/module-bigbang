resource "github_repository_file" "bigbang_yaml" {
  repository          = var.github_project_name
  commit_message      = "chore: update bigbang.yaml"
  file                = "${var.github_environment_dir}/bigbang.yaml"
  branch              = "main"
  overwrite_on_create = true
  // content will be auto base64 encoded
  content = <<-EOT
    apiVersion: v1
    kind: Namespace
    metadata:
      name: bigbang
    ---
    apiVersion: source.toolkit.fluxcd.io/v1beta2
    kind: GitRepository
    metadata:
      name: environment-repo
      namespace: bigbang
    spec:
      interval: 1m
      url: "${var.github_baseurl}${var.github_organization}/${var.github_project_name}"
      ref:
        branch: main
      secretRef:
        name: private-git
    ---
    apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
    kind: Kustomization
    metadata:
      name: environment
      namespace: bigbang
    spec:
      interval: 1m
      sourceRef:
        kind: GitRepository
        name: environment-repo
      path: ./${var.github_environment_dir}
      prune: true
    EOT
}

resource "github_repository_file" "kustomization_yaml" {
  repository          = var.github_project_name
  commit_message      = "chore: modify ${var.github_environment_dir}/kustomization.yaml"
  file                = "${var.github_environment_dir}/kustomization.yaml"
  branch              = "main"
  overwrite_on_create = true
  // content will be auto base64 encoded
  content = <<-EOT
bases:
- git::https://repo1.dso.mil/platform-one/big-bang/bigbang.git//base?ref=${var.bigbang_version}
configMapGenerator:
  - name: environment
    behavior: merge
    files:
      - values.yaml=configmap.yaml
  - name: common
    behavior: merge
    files:
      - values.yaml=configmap.yaml
patchesStrategicMerge:
- |-
  apiVersion: source.toolkit.fluxcd.io/v1beta2
  kind: GitRepository
  metadata:
    name: bigbang
  spec:
    interval: 1m
# Use the following three lines to test a new version of Big Bang without affecting other environments
   # ref:
   #   $patch: replace
   #   tag: "${var.bigbang_version}"
- |-
  apiVersion: helm.toolkit.fluxcd.io/v2beta1
  kind: HelmRelease
  metadata:
    name: bigbang
  spec:
    interval: 1m
  EOT
}

resource "github_repository_file" "configmap_yaml" {
  repository          = var.github_project_name
  commit_message      = "chore: modify ${var.github_environment_dir}/configmap.yaml"
  file                = "${var.github_environment_dir}/configmap.yaml"
  branch              = "main"
  overwrite_on_create = true
  content             = <<-EOT
domain: ${var.domain_name}

networkPolicies:
  enabled: false
sso:
  url: https://keycloak${var.separator}${var.developer}.${var.domain_name}/auth/realms/${var.realm_name}
istiooperator:
  enabled: ${var.bigbang_services["istiooperator"]}
istio:
  ingressGateways:
    public-ingressgateway:
      type: "NodePort"
      nodePortBase: 30000
    passthrough-ingressgateway:
      type: "NodePort"
      nodePortBase: 30100
  gateways:
    passthrough:
      ingressGateway: "passthrough-ingressgateway"
      hosts:
        - "keycloak${var.separator}${var.developer}.${var.domain_name}"
        - "vault${var.separator}${var.developer}.${var.domain_name}"
      tls:
        mode: "PASSTHROUGH"
        
gatekeeper:
  enabled: ${var.bigbang_services["gatekeeper"]}
  values:
    replicas: 1
    controllerManager:
      resources:
        requests:
          cpu: 100m
          memory: 256Mi
    audit:
      resources:
        requests:
          cpu: 200m
          memory: 256Mi
    violations:
      allowedDockerRegistries:
        parameters:
          repos:
          ${indent(10, "- ${join("\n- ", var.gatekeeper_allowed_repos)}")}

addons:
  argocd:
    enabled: ${var.bigbang_services["argocd"]}
    values:
      istio:
        argocd:
          hosts:
            - argocd${var.separator}${var.developer}.${var.domain_name}
  gitlab:
    enabled: ${var.bigbang_services["gitlab"]}
    hostnames:
      gitlab: gitlab${var.separator}${var.developer}
      registry: registry${var.separator}${var.developer}
    values:
      global:
        appConfig:
          backups:
            bucket: bf-gitlab-backup-storage
            tmpBucket: bf-gitlab-tmp-storage
        omniauth:
          enabled: true
          # autoSignInWithProvider:
          # syncProfileFromProvider: []
          syncProfileAttributes: ['email']
          allowSingleSignOn: ['openid_connect']
          blockAutoCreatedUsers: false
          # autoLinkLdapUser: false
          # autoLinkSamlUser: false
          # externalProviders: []
          # allowBypassTwoFactor: []
          providers:
            - secret: gitlab-sso-provider
              key: gitlab-sso.json
      gitlab:
        toolbox:
          backups:
            persistence:
              enabled: true
              accessMode: ReadWriteOnce
              size: 50Gi
            objectStorage:
              backend: s3
              config:
                secret: storage-config
                key: config
            cron:
              enabled: true
              schedule: "0 0 * * *"
    sso:
      enabled: true
      client_id: dev_00eb8904-5b88-4c68-ad67-cec0d2e07aa6_gitlab
      label: "BF SSO"
      client_secret: ${var.gitlab_client_secret}
  gitlabRunner:
    enabled: ${var.bigbang_services["gitlab_runner"]}
  sonarqube:
    enabled: ${var.bigbang_services["sonarqube"]}
    sso:
      enabled: true
      group: ""
      client_id: dev_00eb8904-5b88-4c68-ad67-cec0d2e07aa6_saml-sonarqube
      provider_name: "BF SSO"
      certificate: ${var.sso_certificate}
    values:
      istio:
        sonarqube:
          hosts:
            - sonarqube${var.separator}${var.developer}.${var.domain_name}
      image:
        repository: "registry1.dso.mil/ironbank/sonarsource/sonarqube/sonarqube8-developer"
        tag: "8.9.9-developer"
      plugins:
        image: "registry1.dso.mil/ironbank/sonarsource/sonarqube/sonarqube8-developer:8.9.9-developer"
  nexus:
    enabled: ${var.bigbang_services["nexus"]}
    values:
      hostname: nexus${var.separator}${var.developer}
      persistence:
        storageSize: 200Gi
  vault:
    enabled: ${var.bigbang_services["vault"]}
    ingress:
      gateway: "passthrough"
      key: |
        ${indent(8, var.tls_key)}
      cert: |
        ${indent(8, var.combined_pem)}
    values:
      global:
        tlsDisable: false
      istio:
        vault:
          hosts:
            - vault${var.separator}${var.developer}.${var.domain_name}
      server:
        ingress:
          enabled: false
        extraEnvironmentVars:
          VAULT_SKIP_VERIFY: "true"
          VAULT_LOG_FORMAT: "json"
          VAULT_LICENSE: ${var.vault_license}
        ha:
          enabled: true
          replicas: 3
          apiAddr: https://vault${var.separator}${var.developer}.${var.domain_name}
          
  velero:
    enabled: ${var.bigbang_services["velero"]}
    plugins:
    - aws
    values:
      Eg:
      schedules:
        mybackup:
          disabled: true
          labels:
            myenv: foo
          annotations:
            myenv: foo
          schedule: "*/5 * * * *"
          useOwnerReferencesInBackup: false
          template:
            ttl: "240h"
            includedNamespaces:
            - foo
      credentials:
        existingSecret: cloud-credentials
      configuration:
        provider: aws
        backupStorageLocation:
          bucket: dbp-dev-velero 
          config:
            region: us-gov-east-1
        volumeSnapshotLocation:
          config:
            region: us-gov-east-1
  keycloak:
    enabled: ${var.bigbang_services["keycloak"]}
    ingress:
      gateway: "passthrough"
      key: |
        ${indent(8, var.tls_key)}
      cert: |
        ${indent(8, var.combined_pem)}
    values:
      command:
        - "/opt/keycloak/bin/kc.sh"
      args:
        - "start-dev"
        - "--import-realm"
      istio:
        keycloak:
          hosts:
            - keycloak${var.separator}${var.developer}.${var.domain_name}
      replicas: 1
      extraVolumes: |-
        - name: realm
          secret:
            secretName: realm
        - name: plugin
          emptyDir: {}
        - name: quarkusproperties
          secret:
            secretName: {{ include "keycloak.fullname" . }}-quarkusproperties
            defaultMode: 0777

      extraVolumeMounts: |-
        - name: realm
          mountPath: /opt/keycloak/data/import/realm.json
          subPath: realm.json
          readOnly: true
        - name: plugin
          mountPath: /opt/keycloak/providers/p1-keycloak-plugin.jar
          subPath: p1-keycloak-plugin.jar
        - name: quarkusproperties
          mountPath: /opt/keycloak/conf/quarkus.properties
          subPath: quarkus.properties

      secrets:
        quarkusproperties:
          stringData:
            quarkus.properties: |-
              quarkus.http.non-application-root-path=/
              # custom redirects
              quarkus.kc-routing.path-redirect./=/auth/realms/bdsf/account
              quarkus.kc-routing.path-redirect./auth=/auth/realms/bdsf/account
              quarkus.kc-routing.path-redirect./register=/auth/realms/bdsf/protocol/openid-connect/registrations?client_id=account&response_type=code
              quarkus.kc-routing.path-prefix./oauth/authorize=/auth/realms/bdsf/protocol/openid-connect/auth
              quarkus.kc-routing.path-filter./api/v4/user=/auth/realms/bdsf/protocol/openid-connect/userinfo
              quarkus.kc-routing.path-filter./oauth/token=/auth/realms/bdsf/protocol/openid-connect/token
              # block metrics and health enpoints from being exposed through the istio ingress
              quarkus.kc-routing.path-recursive-block./metrics=8443
              quarkus.kc-routing.path-recursive-block./health=8443

      extraInitContainers: |-
        - name: plugin
          image: registry1.dso.mil/ironbank/big-bang/p1-keycloak-plugin:3.1.0
          imagePullPolicy: Always
          command:
          - sh
          - -c
          - | 
            cp /app/p1-keycloak-plugin.jar /init
            ls -l /init
          volumeMounts:
          - name: plugin
            mountPath: "/init"
      extraEnv: |
        - name: KEYCLOAK_IMPORT
          value: /realm/realm.json
        - name: KC_HTTPS_CERTIFICATE_FILE
          value: /opt/keycloak/conf/tls.crt
        - name: KC_HTTPS_CERTIFICATE_KEY_FILE
          value: /opt/keycloak/conf/tls.key
        - name: KC_HTTPS_CLIENT_AUTH
          value: request
        - name: KC_PROXY
          value: passthrough
        - name: KC_HTTP_ENABLED
          value: "true"
        - name: KC_HTTP_RELATIVE_PATH
          value: /auth
        - name: KC_HOSTNAME
          value: keycloak${var.separator}${var.developer}.${var.domain_name}
        - name: KC_HOSTNAME_STRICT
          value: "false"
        - name: KC_HOSTNAME_STRICT_HTTPS
          value: "false"
        - name: KC_LOG_LEVEL
          value: "org.keycloak.events:DEBUG,org.infinispan:INFO,org.jgroups:INFO"
        - name: KC_CACHE
          value: ispn
        - name: KC_CACHE_STACK
          value: kubernetes


twistlock:
  enabled: ${var.bigbang_services["twistlock"]}
  sso:
    enabled: true
  values:
    istio:
      console:
        hosts:
          - twistlock${var.separator}${var.developer}.${var.domain_name}
    console:
      persistence:
        size: 20Gi

flux:
  interval: 1m
  rollback:
    cleanupOnFail: false

logging:
  enabled: ${var.bigbang_services["logging"]}
  values:
    istio:
      kibana:
        hosts:
          - kibana${var.separator}${var.developer}.${var.domain_name}
    elasticsearch:
      master:
        count: 1
        persistence:
          size: 5Gi
      data:
        count: 1
        persistence:
          size: 10Gi
    kibana:
      count: 1

monitoring:
  enabled: ${var.bigbang_services["monitoring"]}
  values:
    prometheus:
        prometheusSpec:
          resources:
            requests:
              cpu: 200m
              memory: 1Gi
    istio:
      prometheus:
        hosts:
          - prometheus${var.separator}${var.developer}.${var.domain_name}
      alertmanager:
        hosts:
          - alertmanager${var.separator}${var.developer}.${var.domain_name}
      grafana:
        hosts:
          - grafana${var.separator}${var.developer}.${var.domain_name}
      
eckoperator:
  enabled: ${var.bigbang_services["eckoperator"]}

fluentbit:
  enabled: ${var.bigbang_services["fluentbit"]}
  values:
    securityContext:
      privileged: true

clusterAuditor:
  enabled: ${var.bigbang_services["clusterauditor"]}

kiali:
  enabled: true
  values:
    istio:
      kiali:
        hosts:
          - kiali${var.separator}${var.developer}.${var.domain_name}

jaeger:
  enabled: ${var.bigbang_services["jaeger"]}
  values:
    istio:
      jaeger:
        hosts:
          - tracing${var.separator}${var.developer}.${var.domain_name}

kyverno:
  values:
    replicaCount: 1
EOT
}
