variable "bigbang_services" {
  description = "Map of bigbang services we want to deploy (false or true)"
  type        = map(any)
}

variable "bigbang_version" {
  description = "Version of BigBang to deploy"
  type        = string
}

variable "combined_pem" {
  description = "Both TLS private key and CA cert combined"
  type        = string
} 

variable "developer" {
  description = "Person running this terraform code"
  type        = string
  default     = ""
}

variable "domain_name" {
  description = "Domain name for your services (e.g. 'bigbang.dev')"
  type        = string
}

variable "gatekeeper_allowed_repos" {
  description = "Repositories gatekeeper will allow reaching out too"
  type        = list(string)
}

variable "github_baseurl" {
  description = "Github url (e.g. 'https://github.boozallencsn.com')"
  type        = string
}

variable "github_environment_dir" {
  description = "Github directory to place your config files"
  type        = string
}

variable "github_organization" {
  description = "Github org within Github to look in"
  type        = string
}

variable "github_project_name" {
  description = "Name of the project within the Github org"
  type        = string
}

variable "github_token" {
  description = "Github Token"
  type        = string
}

variable "github_username" {
  description = "Github Username"
  type        = string
}

variable "registry_username" {
  description = "Registry1 username"
  type        = string
}

variable "registry_password" {
  description = "Registry1 password"
  type        = string
}

variable "registry_server" {
  description = "Registry1 server"
  type        = string
}

variable "separator" {
  description = "Separator character if this is development for our virtual services"
  type        = string
}

variable "sso_certificate" {
  default = "SSO cert for Sonarqube, Twistlock, etc. from Keycloak"
  type    = string
}

variable "tls_key" {
  description = "TLS private key"
  type        = string
} 

variable "vault_license" {
  description = "License for vault"
  default     = ""
  type        = string
}

variable "realm_name" {
  description = "keycloak realm for deployment"
  type        = string
}

variable "gitlab_client_secret" {
  description = "keycloak realm for deployment"
  type        = string
}
