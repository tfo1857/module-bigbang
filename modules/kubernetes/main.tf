resource "kubectl_manifest" "big-bang-yaml" {
  yaml_body = <<YAML
apiVersion: v1
kind: Namespace
metadata:
  name: bigbang
YAML
}
resource "kubectl_manifest" "big-bang-gitrepo" {
  yaml_body = <<YAML
apiVersion: source.toolkit.fluxcd.io/v1beta2
kind: GitRepository
metadata:
  name: environment-repo
  namespace: bigbang
spec:
  interval: 1m
  url: "${var.github_baseurl}${var.github_organization}/${var.github_project_name}"
  ref:
    branch: main
  secretRef:
    name: private-git
YAML
}

resource "kubectl_manifest" "big-bang-kustomization" {
  yaml_body = <<YAML
apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
kind: Kustomization
metadata:
  name: environment
  namespace: bigbang
spec:
  interval: 1m
  sourceRef:
    kind: GitRepository
    name: environment-repo
  path: ./${var.github_environment_dir}
  prune: true
YAML
}