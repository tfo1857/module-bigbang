locals {
  cert_ca = var.environment == "development" ? var.bigbang_ca : var.combined_pem
}

resource "kubernetes_secret" "storage-config" {

  depends_on = [kubernetes_namespace.gitlab]

  metadata {
    name      = "storage-config"
    namespace = "gitlab"
  }

  data = {
    "config" = <<-EOT
      [default]
      access_key = ${var.aws_access_key}
      secret_key = ${var.aws_secret_key}
      bucket_location = ${var.aws_default_region}
      host_base = s3-${var.aws_default_region}.amazonaws.com
      add_encoding_exts =
      add_headers =
      ca_certs_file =
      cache_file =
      check_ssl_certificate = True
      check_ssl_hostname = True
      cloudfront_host = cloudfront.amazonaws.com
      connection_max_age = 5
      connection_pooling = True
      content_disposition =
      content_type =
      default_mime_type = binary/octet-stream
      delay_updates = False
      delete_after = False
      delete_after_fetch = False
      delete_removed = False
      dry_run = False
      enable_multipart = True
      encoding = UTF-8
      encrypt = False
      expiry_date =
      expiry_days =
      expiry_prefix =
      follow_symlinks = False
      force = False
      get_continue = False
      gpg_command = /usr/bin/gpg
      gpg_decrypt = %(gpg_command)s -d --verbose --no-use-agent --batch --yes --passphrase-fd %(passphrase_fd)s -o %(output_file)s %(input_file)s
      gpg_encrypt = %(gpg_command)s -c --verbose --no-use-agent --batch --yes --passphrase-fd %(passphrase_fd)s -o %(output_file)s %(input_file)s
      gpg_passphrase =
      guess_mime_type = True
      host_bucket = %(bucket)s.s3-${var.aws_default_region}.amazonaws.com
      human_readable_sizes = False
      invalidate_default_index_on_cf = False
      invalidate_default_index_root_on_cf = True
      invalidate_on_cf = False
      kms_key =
      limit = -1
      limitrate = 0
      list_md5 = False
      log_target_prefix =
      long_listing = False
      max_delete = -1
      mime_type =
      multipart_chunk_size_mb = 15
      multipart_copy_chunk_size_mb = 1024
      multipart_max_chunks = 10000
      preserve_attrs = True
      progress_meter = True
      proxy_host =
      proxy_port = 0
      public_url_use_https = False
      put_continue = False
      recursive = False
      recv_chunk = 65536
      reduced_redundancy = False
      requester_pays = False
      restore_days = 1
      restore_priority = Standard
      send_chunk = 65536
      server_side_encryption = False
      signature_v2 = False
      signurl_use_https = False
      simpledb_host = sdb.amazonaws.com
      skip_existing = False
      socket_timeout = 300
      ssl_client_cert_file =
      ssl_client_key_file =
      stats = False
      stop_on_error = False
      storage_class =
      throttle_max = 100
      upload_id =
      urlencoding_mode = normal
      use_http_expect = False
      use_https = True
      use_mime_magic = True
      verbosity = WARNING
      website_endpoint = http://%(bucket)s.s3-website-%(location)s.amazonaws.com/
      website_error =
      website_index = index.html
      EOT
    filename = ".s3cfg"
  }
}

resource "kubernetes_secret" "terraform" {
  depends_on = [kubectl_manifest.big-bang-yaml]
  metadata {
    name      = "terraform"
    namespace = "bigbang"
  }

  data = {
    "values.yaml" = <<-EOT
      sso:
        certificate_authority: |-
          ${indent(4, "${local.cert_ca}")}
      registryCredentials:
      - registry: ${var.registry_server}
        username: ${var.registry_username}
        password: ${var.registry_password}
      istio:
        gateways:
          public:
            tls:
              key: |-
                ${indent(10, "${var.tls_key}")}
              cert: |-
                ${indent(10, "${var.combined_pem}")}
    EOT
  }
}


resource "kubernetes_secret" "private-git" {
  depends_on = [kubectl_manifest.big-bang-yaml]
  metadata {
    name      = "private-git"
    namespace = "bigbang"
  }
  data = {
    username = "${var.github_username}"
    password = "${var.github_token}"
  }
}

resource "kubernetes_secret" "vault-license" {
  metadata {
    name      = "vault-license"
    namespace = "vault"
  }
  data = {
    license = "${var.vault_license}"
  }
}

resource "kubernetes_secret" "registry_key_jfrog" {
  depends_on = [kubernetes_namespace.jfrog]
  metadata {
    name      = "regcred"
    namespace = "jfrog"
  }
  type = "kubernetes.io/dockerconfigjson"
  data = {
    ".dockerconfigjson" = jsonencode({
      auths = {
        "registry1.dso.mil" = {
          "username" = "${var.registry_username}"
          "password" = "${var.registry_password}"
          "auth"     = base64encode("${var.registry_username}:${var.registry_password}")
        }
      }
    })
  }
}