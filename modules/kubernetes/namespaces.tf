locals {
  val_hooks = tostring(join(" ", var.validating_hooks))
  mut_hooks = tostring(join(" ", var.mutating_hooks))
}

resource "null_resource" "crd_delete" {
  depends_on = [kubernetes_namespace.namespace_bigbang, kubernetes_namespace.kiali, kubernetes_namespace.istio-system]
  triggers = {
    bucket     = var.bucket
    kubeconfig = var.kubeconfig
    bucket_dir = var.bucket_dir
  }

  provisioner "local-exec" {
    interpreter = ["bash", "-c"]
    command     = "echo 'This is a filler local-exec so the resource can be created and thus destroyed and delete the CRDs'"
  }

  provisioner "local-exec" {
    when        = destroy
    interpreter = ["bash", "-c"]
    command     = <<EOF
        aws s3 cp s3://${self.triggers.bucket}/${self.triggers.bucket_dir}/${self.triggers.kubeconfig} .
        export KUBECONFIG="${self.triggers.kubeconfig}"
        kubectl patch kiali.kiali.io/kiali -n kiali --type json --patch='[ { "op": "remove", "path": "/metadata/finalizers" } ]' || true
        kubectl patch istiooperator.install.istio.io/istiocontrolplane -n istio-system --type json --patch='[ { "op": "remove", "path": "/metadata/finalizers" } ]' || true
        rm ${self.triggers.kubeconfig} || true
    EOF
  }
}

resource "null_resource" "hook_delete" {
  depends_on = [kubernetes_namespace.namespace_bigbang, null_resource.crd_delete]
  triggers = {
    val_hooks  = local.val_hooks
    mut_hooks  = local.mut_hooks
    bucket     = var.bucket
    kubeconfig = var.kubeconfig
    bucket_dir = var.bucket_dir
  }

  provisioner "local-exec" {
    interpreter = ["bash", "-c"]
    command     = "echo 'This is a filler local-exec so the resource can delete mutating and validating webhooks upon deletion!'"
  }

  provisioner "local-exec" {
    when        = destroy
    interpreter = ["bash", "-c"]
    command     = <<EOF
        aws s3 cp s3://${self.triggers.bucket}/${self.triggers.bucket_dir}/${self.triggers.kubeconfig} .
        export KUBECONFIG="${self.triggers.kubeconfig}"
        kubectl delete validatingwebhookconfigurations ${self.triggers.val_hooks} --ignore-not-found=true
        kubectl delete mutatingwebhookconfigurations ${self.triggers.mut_hooks} --ignore-not-found=true
        rm ${self.triggers.kubeconfig} || true
    EOF
  }
}

resource "kubernetes_namespace" "namespace_bigbang" {
  metadata {
    name = "bigbang"
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }

}

resource "kubernetes_namespace" "jfrog" {
  metadata {
    name = "jfrog"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }

}

resource "kubernetes_namespace" "gitlab" {
  metadata {
    name = "gitlab"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "argocd" {
  metadata {
    name = "argocd"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "cluster-auditor" {
  metadata {
    name = "cluster-auditor"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "gatekeeper-system" {
  metadata {
    name = "gatekeeper-system"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "vault" {
  metadata {
    name = "vault"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "istio-system" {
  metadata {
    name = "istio-system"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "istio-operator" {
  depends_on = [kubernetes_namespace.istio-system]
  metadata {
    name = "istio-operator"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "jaeger" {
  metadata {
    name = "jaeger"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "kiali" {
  metadata {
    name = "kiali"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "eck-operator" {
  metadata {
    name = "eck-operator"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "logging" {
  metadata {
    name = "logging"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    name = "monitoring"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "nexus-repository-manager" {
  metadata {
    name = "nexus-repository-manager"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "sonarqube" {
  metadata {
    name = "sonarqube"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "twistlock" {
  metadata {
    name = "twistlock"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}

resource "kubernetes_namespace" "keycloak" {
  metadata {
    name = "keycloak"

    annotations = {
      "meta.helm.sh/release-name"      = "bigbang"
      "meta.helm.sh/release-namespace" = "bigbang"
    }
    labels = {
      "app.kubernetes.io/managed-by" = "Helm"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].labels,
    ]
  }
}