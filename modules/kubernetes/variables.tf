variable "aws_access_key" {
  description = "IAM AWS access key"
  type        = string
}

variable "aws_secret_key" {
  description = "IAM AWS secret key"
  type        = string
}

variable "aws_default_region" {
  description = "Default region running this command"
  type        = string
}

variable "bigbang_ca" {
  description = "Bigbang's bigbang.dev LetsEncrypt certificate authority"
  default     = ""
  type        = string
}

variable "bigbang_version" {
  description = "Version of BigBang to deploy"
  type        = string
}

variable "bucket" {
  description = "S3 bucket used to identify cluster resource"
  type        = string
}

variable "bucket_dir" {
  description = "S3 bucket directory"
  default     = ""
}

variable "combined_pem" {
  description = "Both TLS private key and CA cert combined"
  type        = string
}

variable "developer" {
  description = "Developer that is running this terraform code"
  type        = string
  default     = ""
}

variable "domain_name" {
  description = "Domain name for your services (e.g. 'bigbang.dev')"
  type        = string
}

variable "environment" {
  description = "Environment we're deploying too (development, production, etc)"
  type        = string
  default     = "development"
}

variable "gatekeeper_allowed_repos" {
  description = "Repositories gatekeeper will allow reaching out too"
  type        = list(string)
}

variable "github_baseurl" {
  description = "Github url (e.g. 'https://github.boozallencsn.com')"
  type        = string
}

variable "github_environment_dir" {
  description = "Github directory to place your config files"
  type        = string
}

variable "github_organization" {
  description = "Github org within Github to look in"
  type        = string
}

variable "github_project_name" {
  description = "Name of the project within the Github org"
  type        = string
}

variable "github_token" {
  description = "Github Token"
  type        = string
}

variable "github_username" {
  description = "Github Username"
  type        = string
}

variable "kubeconfig" {
  description = "Name of the kubeconfig file located in s3"
  default     = ""
}

variable "mutating_hooks" {
  description = "Mutating webhooks we want to delete when we destroy BigBang"
  type        = list(string)
  default = [
    "jaeger-operator-mutating-webhook-configuration",
    "monitoring-monitoring-kube-admission",
  ]
}

variable "registry_password" {
  description = "Registry1 password"
  type        = string
}

variable "registry_username" {
  description = "Registry1 username"
  type        = string
}

variable "registry_server" {
  description = "Registry1 server"
  type        = string
}

variable "sso_certificate" {
  default = "SSO cert for Sonarqube, Twistlock, etc. from Keycloak"
  type    = string
}

variable "separator" {
  description = "Separator used in domain names if launching in development environment"
  type        = string
}

variable "tls_key" {
  description = "TLS private key"
  type        = string
}

variable "validating_hooks" {
  description = "Validating webhooks we want to delete when we destroy BigBang"
  type        = list(string)
  default = [
    "gatekeeper-validating-webhook-configuration",
    "elastic-operator.eck-operator.k8s.elastic.co",
    "jaeger-operator-validating-webhook-configuration",
    "monitoring-monitoring-kube-admission",
  ]
}

variable "vault_license" {
  description = "Vault license"
  type        = string
}
