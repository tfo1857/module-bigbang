## I'd like to keep this as an example of how to install bigbang with helm_release


# resource "helm_release" "bigbang" {
#   name             = "bigbang"
#   chart            = "${path.module}/chart/bigbang-${var.bigbang_version}"
#   namespace        = "bigbang"
#   create_namespace = true

#   values = [
#     templatefile("${path.module}/values-tmpl.yaml", {
#       cert_pem                 = "${var.tls_crt}"
#       private_key              = "${var.tls_key}"
#       registry_server          = "${var.registry_server}"
#       registry_username        = "${var.registry_username}"
#       registry_password        = "${var.registry_password}"
#       cluster_domain           = "${var.cluster_domain}"
#       gatekeeper_allowed_repos = "${var.gatekeeper_allowed_repos}"
#       sso_certificate          = "${var.sso_certificate}"
#     }),
#     file("./ingress-certs.yaml")
#   ]
# }
