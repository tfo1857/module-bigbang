resource "kubernetes_secret" "private-registry" {
  depends_on = [kustomization_resource.p2, time_sleep.wait_for_flux_namespace]
  metadata {
    name      = "private-registry"
    namespace = "flux-system"
  }

  type = "kubernetes.io/dockerconfigjson"

  data = {
    ".dockerconfigjson" = jsonencode({
      auths = {
        "${var.registry_server}" = {
          "username" = var.registry_username
          "password" = var.registry_password
          "auth"     = base64encode("${var.registry_username}:${var.registry_password}")
        }
      }
    })
  }
}

resource "time_sleep" "wait_for_flux_namespace" {
  create_duration = "10s"
}