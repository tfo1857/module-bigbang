variable "bigbang_version" {
  description = "Version of BigBang to deploy"
  type        = string
}

variable "registry_username" {
  description = "Registry1 username"
  type        = string
}

variable "registry_password" {
  description = "Registry1 password"
  type        = string
}

variable "registry_server" {
  description = "Registry1 server"
  type        = string
}
