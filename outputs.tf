output "gitops_url_location" {
  description = "URL to our gitops directory within Github; it contains our configmap, bigbang and kustomization yamls for this deployment"
  value       = "${var.github_baseurl}${var.github_organization}/${var.github_project_name}/tree/main/${var.github_environment_dir}"
}