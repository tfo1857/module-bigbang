variable "aws_access_key" {
  description = "IAM AWS access key"
  type        = string
}
variable "aws_secret_key" {
  description = "IAM AWS secret key"
  type        = string
}
variable "aws_default_region" {
  description = "Default region running this command"
  type        = string
}
variable "bigbang_ca" {
  description = "Bigbang's bigbang.dev LetsEncrypt certificate authority"
  type        = string
  default     = <<EOF
  -----BEGIN CERTIFICATE-----
  MIIFazCCA1OgAwIBAgIRAIIQz7DSQONZRGPgu2OCiwAwDQYJKoZIhvcNAQELBQAw
  TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh
  cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMTUwNjA0MTEwNDM4
  WhcNMzUwNjA0MTEwNDM4WjBPMQswCQYDVQQGEwJVUzEpMCcGA1UEChMgSW50ZXJu
  ZXQgU2VjdXJpdHkgUmVzZWFyY2ggR3JvdXAxFTATBgNVBAMTDElTUkcgUm9vdCBY
  MTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAK3oJHP0FDfzm54rVygc
  h77ct984kIxuPOZXoHj3dcKi/vVqbvYATyjb3miGbESTtrFj/RQSa78f0uoxmyF+
  0TM8ukj13Xnfs7j/EvEhmkvBioZxaUpmZmyPfjxwv60pIgbz5MDmgK7iS4+3mX6U
  A5/TR5d8mUgjU+g4rk8Kb4Mu0UlXjIB0ttov0DiNewNwIRt18jA8+o+u3dpjq+sW
  T8KOEUt+zwvo/7V3LvSye0rgTBIlDHCNAymg4VMk7BPZ7hm/ELNKjD+Jo2FR3qyH
  B5T0Y3HsLuJvW5iB4YlcNHlsdu87kGJ55tukmi8mxdAQ4Q7e2RCOFvu396j3x+UC
  B5iPNgiV5+I3lg02dZ77DnKxHZu8A/lJBdiB3QW0KtZB6awBdpUKD9jf1b0SHzUv
  KBds0pjBqAlkd25HN7rOrFleaJ1/ctaJxQZBKT5ZPt0m9STJEadao0xAH0ahmbWn
  OlFuhjuefXKnEgV4We0+UXgVCwOPjdAvBbI+e0ocS3MFEvzG6uBQE3xDk3SzynTn
  jh8BCNAw1FtxNrQHusEwMFxIt4I7mKZ9YIqioymCzLq9gwQbooMDQaHWBfEbwrbw
  qHyGO0aoSCqI3Haadr8faqU9GY/rOPNk3sgrDQoo//fb4hVC1CLQJ13hef4Y53CI
  rU7m2Ys6xt0nUW7/vGT1M0NPAgMBAAGjQjBAMA4GA1UdDwEB/wQEAwIBBjAPBgNV
  HRMBAf8EBTADAQH/MB0GA1UdDgQWBBR5tFnme7bl5AFzgAiIyBpY9umbbjANBgkq
  hkiG9w0BAQsFAAOCAgEAVR9YqbyyqFDQDLHYGmkgJykIrGF1XIpu+ILlaS/V9lZL
  ubhzEFnTIZd+50xx+7LSYK05qAvqFyFWhfFQDlnrzuBZ6brJFe+GnY+EgPbk6ZGQ
  3BebYhtF8GaV0nxvwuo77x/Py9auJ/GpsMiu/X1+mvoiBOv/2X/qkSsisRcOj/KK
  NFtY2PwByVS5uCbMiogziUwthDyC3+6WVwW6LLv3xLfHTjuCvjHIInNzktHCgKQ5
  ORAzI4JMPJ+GslWYHb4phowim57iaztXOoJwTdwJx4nLCgdNbOhdjsnvzqvHu7Ur
  TkXWStAmzOVyyghqpZXjFaH3pO3JLF+l+/+sKAIuvtd7u+Nxe5AW0wdeRlN8NwdC
  jNPElpzVmbUq4JUagEiuTDkHzsxHpFKVK7q4+63SM1N95R1NbdWhscdCb+ZAJzVc
  oyi3B43njTOQ5yOf+1CceWxG1bQVs5ZufpsMljq4Ui0/1lvh+wjChP4kqKOJ2qxq
  4RgqsahDYVvTH9w7jXbyLeiNdd8XM2w9U/t7y0Ff/9yi0GE44Za4rF2LN9d11TPA
  mRGunUHBcnWEvgJBQl9nJEiU0Zsnvgc/ubhPgXRR4Xq37Z0j4r7g1SgEEzwxA57d
  emyPxgcYxn/eR44/KJ4EBs+lVDR3veyJm+kXQ99b21/+jh5Xos1AnX5iItreGCc=
  -----END CERTIFICATE-----
  EOF
}

variable "bigbang_services" {
  description = "Map of bigbang services we want to deploy (false or true)"
  type        = map(any)
}

variable "bigbang_version" {
  description = "Version of BigBang to deploy"
  default     = "1.53.0"
  type        = string
}
variable "bucket" {
  description = "S3 bucket used to identify cluster resource"
  type        = string
}
variable "bucket_dir" {
  description = "S3 bucket directory"
  default     = ""
}
variable "combined_pem" {
  description = "Both TLS private key and CA cert combined"
  type        = string
} 
variable "developer" {
  description = "Name of the developer running this module"
  type        = string
  default     = ""
}
variable "domain_name" {
  description = "Domain name for your services (e.g. 'bigbang.dev')"
  type        = string
}
variable "environment" {
  description = "Environment we're deploying too (development, production, etc)"
  type        = string
  default     = "development"
}
variable "gatekeeper_allowed_repos" {
  description = "Repositories gatekeeper will allow reaching out too"
  default = [
    "registry1.dso.mil",
    "registry.dso.mil",
    "docker.io",
    "registry.gitlab.com",
    "gcr.io",
    "releases-docker.jfrog.io",
  ]
  type = list(string)
}
variable "github_username" {
  description = "Github Username"
  type        = string
}
variable "github_token" {
  description = "Github Token"
  type        = string
}
variable "github_baseurl" {
  description = "Github url (e.g. 'https://github.boozallencsn.com')"
  type        = string
}
variable "github_organization" {
  description = "Github org within Github to look in"
  type        = string
}
variable "github_project_name" {
  description = "Name of the project within the Github org"
  type        = string
}
variable "github_environment_dir" {
  description = "Github directory to place your config files"
  type        = string
}
variable "kubeconfig" {
  description = "Name of the kubeconfig file located in s3"
  default     = ""
}
variable "registry_username" {
  description = "Registry1 username"
  type        = string
}
variable "registry_password" {
  description = "Registry1 password"
  type        = string
}
variable "registry_server" {
  description = "Registry1 server"
  default     = "registry1.dso.mil"
  type        = string
}
variable "sso_certificate" {
  default = "SSO cert for Sonarqube, Twistlock, etc. from Keycloak"
  type    = string
}

variable "tls_key" {
  description = "TLS private key"
  type        = string
} 

variable "vault_license" {
  description = "License for vault"
  default     = ""
  type        = string
}

variable "realm_name" {
  description = "keycloak realm for deployment"
  default     = "bdsf"
  type        = string
}


variable "gitlab_client_secret" {
  description = "keycloak realm for deployment"
  type        = string
}
